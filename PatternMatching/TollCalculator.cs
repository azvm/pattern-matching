﻿using System;
using CommercialRegistration;
using ConsumerVehicleRegistration;
using LiveryRegistration;

namespace PatternMatching
{
    public static class TollCalculator
    {
        private enum TimeWindow
        {
            MorningRush,
            Daytime,
            EveningRush,
            Overnight
        }

        public static decimal CalculateToll(object vehicle, DateTime time, bool inbound)
        {
            return ConsiderTime(vehicle, time, inbound);
        }

        private static decimal GetBaseToll(object vehicle)
        {
            return vehicle switch
            {
                Car           => Tolls.Car.BaseToll,
                Taxi          => Tolls.Taxi.BaseToll,
                Bus           => Tolls.Bus.BaseToll,
                DeliveryTruck => Tolls.DeliveryTruck.BaseToll,
                null          => throw new ArgumentNullException(nameof(vehicle)),

                _ => throw new ArgumentException("Unknown vehicle type.", nameof(vehicle))
            };
        }

        private static decimal ConsiderOccupancy(object vehicle)
        {
            var toll = GetBaseToll(vehicle);

            return vehicle switch
            {
                Car car => car.Passengers switch
                {
                    0    => toll + Tolls.Car.NoPassengersAddend,
                    2    => toll + Tolls.Car.TwoPassengersAddend,
                    >= 3 => toll + Tolls.Car.ThreeOrMorePassengersAddend,

                    _ => toll,
                },

                Taxi taxi => taxi.Fares switch
                {
                    0    => toll + Tolls.Taxi.NoFaresAddend,
                    2    => toll + Tolls.Taxi.TwoFaresAddend,
                    >= 3 => toll + Tolls.Taxi.ThreeOrMoreFaresAddend,

                    _ => toll,
                },

                Bus bus => GetBusOccupancyPercent(bus) switch
                {
                    < 50 => toll + Tolls.Bus.LessThan50PercentFullAddend,
                    > 90 => toll + Tolls.Bus.MoreThan90PercentFullAddend,

                    _ => toll,
                },

                DeliveryTruck truck => truck.GrossWeightClass switch
                {
                    > 5000 => toll + Tolls.DeliveryTruck.Over5000LbsAddend,
                    < 3000 => toll + Tolls.DeliveryTruck.Under3000LbsAddend,

                    _ => toll,
                },

                _ => toll,
            };

            static int GetBusOccupancyPercent(Bus bus) => (int)(bus.Riders / (double)bus.Capacity * 100);
        }

        private static decimal ConsiderTime(object vehicle, DateTime time, bool inbound)
        {
            var toll = ConsiderOccupancy(vehicle);

            return time.DayOfWeek switch
            {
                DayOfWeek.Saturday or DayOfWeek.Sunday => toll,

                _ => GetTimeWindow(time) switch
                {
                    TimeWindow.MorningRush when inbound  => toll * Tolls.WorkdayRushMultiplier,
                    TimeWindow.EveningRush when !inbound => toll * Tolls.WorkdayRushMultiplier,
                    TimeWindow.Daytime                   => toll * Tolls.WorkdayMultiplier,
                    TimeWindow.Overnight                 => toll * Tolls.WorkdayOvernightMultiplier,

                    _ => toll,
                },
            };

            static TimeWindow GetTimeWindow(DateTime time)
            {
                return time.Hour switch
                {
                    < 6 or > 19 => TimeWindow.Overnight,
                    < 10        => TimeWindow.MorningRush,
                    < 16        => TimeWindow.Daytime,

                    _ => TimeWindow.EveningRush,
                };
            }
        }
    }
}
