﻿namespace PatternMatching
{
    public static class Tolls
    {
        public static class Car
        {
            public const decimal BaseToll = 2.00m;

            public const decimal NoPassengersAddend = 0.50m;
            public const decimal TwoPassengersAddend = -0.50m;
            public const decimal ThreeOrMorePassengersAddend = -1.00m;
        }

        public static class Taxi
        {
            public const decimal BaseToll = 3.50m;

            public const decimal NoFaresAddend = 0.50m;
            public const decimal TwoFaresAddend = -0.50m;
            public const decimal ThreeOrMoreFaresAddend = -1.00m;
        }

        public static class Bus
        {
            public const decimal BaseToll = 5.00m;

            public const decimal LessThan50PercentFullAddend = 2.00m;
            public const decimal MoreThan90PercentFullAddend = -1.00m;
        }

        public static class DeliveryTruck
        {
            public const decimal BaseToll = 10.00m;

            public const decimal Over5000LbsAddend = 5.00m;
            public const decimal Under3000LbsAddend = -2.00m;
        }

        public const decimal WorkdayRushMultiplier = 2.00m;
        public const decimal WorkdayMultiplier = 1.50m;
        public const decimal WorkdayOvernightMultiplier = 0.75m;
    }
}
