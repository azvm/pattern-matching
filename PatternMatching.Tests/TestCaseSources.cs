﻿using System;
using System.Collections.Generic;
using CommercialRegistration;
using ConsumerVehicleRegistration;
using LiveryRegistration;
using NUnit.Framework;

namespace PatternMatching.Tests
{
    public static class TestCaseSources
    {
        private static Car SoloDriver     => new Car();
        private static Car TwoRideShare   => new Car { Passengers = 1 };
        private static Car ThreeRideShare => new Car { Passengers = 2 };
        private static Car FullVan        => new Car { Passengers = 5 };

        private static Taxi EmptyTaxi   => new Taxi();
        private static Taxi SingleFare  => new Taxi { Fares = 1 };
        private static Taxi DoubleFare  => new Taxi { Fares = 2 };
        private static Taxi FullVanPool => new Taxi { Fares = 5 };

        private static Bus LowOccupantBus => new Bus { Capacity = 90, Riders = 15 };
        private static Bus NormalBus      => new Bus { Capacity = 90, Riders = 75 };
        private static Bus FullBus        => new Bus { Capacity = 90, Riders = 85 };

        private static DeliveryTruck HeavyTruck => new DeliveryTruck { GrossWeightClass = 7500 };
        private static DeliveryTruck Truck      => new DeliveryTruck { GrossWeightClass = 4000 };
        private static DeliveryTruck LightTruck => new DeliveryTruck { GrossWeightClass = 2500 };

        private static DateTime MorningRush        => new DateTime(2019, 3, 4, 8, 0, 0);
        private static DateTime EveningRush        => new DateTime(2019, 3, 7, 17, 15, 0);
        private static DateTime Daytime            => new DateTime(2019, 3, 6, 11, 30, 0);
        private static DateTime Overnight          => new DateTime(2019, 3, 14, 03, 30, 0);
        private static DateTime WeekendMorningRush => new DateTime(2019, 3, 16, 8, 30, 0);
        private static DateTime WeekendEveningRush => new DateTime(2019, 3, 17, 18, 05, 0);
        private static DateTime WeekendDaytime     => new DateTime(2019, 3, 17, 14, 30, 0);
        private static DateTime WeekendOvernight   => new DateTime(2019, 3, 16, 01, 30, 0);

        public static IEnumerable<TestCaseData> Car
        {
            get
            {
                yield return new TestCaseData(SoloDriver, WeekendDaytime, true, Tolls.Car.BaseToll + Tolls.Car.NoPassengersAddend);
                // TODO: Add test cases
            }
        }

        public static IEnumerable<TestCaseData> Taxi
        {
            get
            {
                yield return new TestCaseData(EmptyTaxi, WeekendEveningRush, false, Tolls.Taxi.BaseToll + Tolls.Taxi.NoFaresAddend);
                // TODO: Add test cases
            }
        }

        public static IEnumerable<TestCaseData> Bus
        {
            get
            {
                yield return new TestCaseData(LowOccupantBus, WeekendOvernight, true, Tolls.Bus.BaseToll + Tolls.Bus.LessThan50PercentFullAddend);
                // TODO: Add test cases
            }
        }

        public static IEnumerable<TestCaseData> DeliveryTruck
        {
            get
            {
                yield return new TestCaseData(HeavyTruck, WeekendMorningRush, false, Tolls.DeliveryTruck.BaseToll + Tolls.DeliveryTruck.Over5000LbsAddend);
                // TODO: Add test cases
            }
        }
    }
}
