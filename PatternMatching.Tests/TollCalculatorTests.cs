﻿using System;
using CommercialRegistration;
using ConsumerVehicleRegistration;
using LiveryRegistration;
using NUnit.Framework;

namespace PatternMatching.Tests
{
    [TestFixture]
    public class TollCalculatorTests
    {
        [Test]
        public void CalculateToll_VehicleIsNull_ThrowsArgumentNullException()
        {
            Assert.Throws<ArgumentNullException>(() => TollCalculator.CalculateToll(null, DateTime.Now, false));
        }

        [Test]
        public void CalculateToll_UnknownVehicle_ThrowsArgumentException()
        {
            Assert.Throws<ArgumentException>(() => TollCalculator.CalculateToll(new object(), DateTime.Now, false));
            Assert.Throws<ArgumentException>(() => TollCalculator.CalculateToll(int.MaxValue, DateTime.Now, false));
            Assert.Throws<ArgumentException>(() => TollCalculator.CalculateToll("Car", DateTime.Now, false));
            Assert.Throws<ArgumentException>(() => TollCalculator.CalculateToll(new { x = 0 }, DateTime.Now, false));
        }

        [TestCaseSource(typeof(TestCaseSources), nameof(TestCaseSources.Car))]
        public void CalculateToll_Car_CalculatesTollCorrectly(Car car, DateTime time, bool inbound, decimal expected)
        {
            var actual = TollCalculator.CalculateToll(car, time, inbound);

            Assert.AreEqual(expected, actual);
        }

        [TestCaseSource(typeof(TestCaseSources), nameof(TestCaseSources.Taxi))]
        public void CalculateToll_Taxi_CalculatesTollCorrectly(Taxi taxi, DateTime time, bool inbound, decimal expected)
        {
            var actual = TollCalculator.CalculateToll(taxi, time, inbound);

            Assert.AreEqual(expected, actual);
        }

        [TestCaseSource(typeof(TestCaseSources), nameof(TestCaseSources.Bus))]
        public void CalculateToll_Bus_CalculatesTollCorrectly(Bus bus, DateTime time, bool inbound, decimal expected)
        {
            var actual = TollCalculator.CalculateToll(bus, time, inbound);

            Assert.AreEqual(expected, actual);
        }

        [TestCaseSource(typeof(TestCaseSources), nameof(TestCaseSources.DeliveryTruck))]
        public void CalculateToll_DeliveryTruck_CalculatesTollCorrectly(DeliveryTruck truck, DateTime time, bool inbound, decimal expected)
        {
            var actual = TollCalculator.CalculateToll(truck, time, inbound);

            Assert.AreEqual(expected, actual);
        }
    }
}
